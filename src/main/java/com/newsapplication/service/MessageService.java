package com.newsapplication.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.newsapplication.model.MessageData;
import com.newsapplication.repository.MessageRepositoryInterface;

@Service
public class MessageService implements MessageServiceInterface {
	
	Logger logger = LoggerFactory.getLogger(MessageService.class);
	
	MessageRepositoryInterface messageRepository;
	
	@Autowired
	public void setMessageRepository(MessageRepositoryInterface msgRepo){
		messageRepository = msgRepo;
	}

	@Override
	public void saveMessage(MessageData msgData) throws DataAccessException{
		logger.info("in saveMessage service method");
		messageRepository.saveMessage(msgData);
		
	}

}
