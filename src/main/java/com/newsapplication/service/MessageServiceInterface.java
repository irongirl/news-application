package com.newsapplication.service;

import com.newsapplication.model.MessageData;

public interface MessageServiceInterface {
	public void saveMessage(MessageData msgData);
}
