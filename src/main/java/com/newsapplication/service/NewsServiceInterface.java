package com.newsapplication.service;

import java.util.List;
import java.util.Locale;

import com.newsapplication.model.Article;
import com.newsapplication.model.Category;



public interface NewsServiceInterface {
	//List<Article> getNewsGlobal();
	List<Article> getTopHeadlineNewsByLocale(Locale local);
	List<Article> getTopHeadlineNewsByLocaleAndCategory(Locale local, Category category);
}
