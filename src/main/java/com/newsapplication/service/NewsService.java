package com.newsapplication.service;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.neovisionaries.i18n.CountryCode;
import com.newsapplication.model.Article;
import com.newsapplication.model.Category;
import com.newsapplication.model.ResponseFromGoogleApi;

@Service
public class NewsService implements NewsServiceInterface {
	
	Logger logger = LoggerFactory.getLogger(NewsService.class);
	RestTemplate restTemplate = new RestTemplate();
	
	@Value("${newsapi.baseurl}")
	String baseUrl;
	
	@Value("${newsapi.apiKey}")
	String apiKey;
	
	
	private String convertLocaleToIso31661CountryCode(Locale local) {
		String iso3language =local.getISO3Language() ;
		String language ="hu";
		
		if("eng".equals(iso3language)){
			language ="us";
		} 
		
		return language;
	}

	@Override
	public List<Article> getTopHeadlineNewsByLocale(Locale local) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl)
				.queryParam("country", convertLocaleToIso31661CountryCode(local))
				.queryParam("apiKey", apiKey);	
		ResponseFromGoogleApi responseObj = restTemplate.getForObject(builder.toUriString(), ResponseFromGoogleApi.class);
		logger.info("in getTopHeadlineNewsByLocale service method");
		return responseObj.getArticles();
	}
	
	@Override
	public List<Article> getTopHeadlineNewsByLocaleAndCategory(Locale local, Category category) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl)
				.queryParam("country", convertLocaleToIso31661CountryCode(local))
				.queryParam("category",category )
				.queryParam("apiKey", apiKey);
		ResponseFromGoogleApi responseObj = restTemplate.getForObject(builder.toUriString(), ResponseFromGoogleApi.class);
		logger.info("in getTopHeadlineNewsByLocaleAndCategory service method");
		return responseObj.getArticles();
	}

}
