package com.newsapplication.repository;

import com.newsapplication.model.MessageData;

public interface MessageRepositoryInterface {
	public void saveMessage(MessageData msgData);
}
