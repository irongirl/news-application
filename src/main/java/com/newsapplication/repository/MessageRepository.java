package com.newsapplication.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.newsapplication.model.MessageData;

@Repository
public class MessageRepository implements MessageRepositoryInterface {
	
	Logger logger = LoggerFactory.getLogger(MessageRepository.class);
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public void saveMessage(MessageData msgData) throws DataAccessException{
		logger.info("in saveMessage repository method");
		String sql = "INSERT INTO messages (name, email, message) VALUES (?, ? ,?)";
		
		jdbcTemplate.update(sql, msgData.getName(), msgData.getEmail(), msgData.getMessage());
		
	}

}
