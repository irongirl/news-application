package com.newsapplication.model;

public enum Category {
	business, entertainment, general, health, science, sports, technology
}
