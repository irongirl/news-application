package com.newsapplication.controller;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.newsapplication.model.Article;
import com.newsapplication.model.MessageData;
import com.newsapplication.service.MessageServiceInterface;

@Controller
public class MessageController {
	
	Logger logger = LoggerFactory.getLogger(MessageController.class);
	MessageServiceInterface messageService;
	
	@Autowired
	public void setMessageService(MessageServiceInterface msgService){
		messageService = msgService;
	}
	
	@GetMapping("/contact/writeus")
	public String getContactPage(@ModelAttribute("msgData") MessageData msgData){
		logger.info("in /contact/writeus get handler method ");
		return "contact";
	}
	
	@PostMapping("/contact/writeus")
	public String postContact(@Valid @ModelAttribute("msgData") MessageData msgData, BindingResult bindingResult){
		if (bindingResult.hasErrors()){
			return "contact";
		}
	
		try {
			messageService.saveMessage(msgData);
		} catch (DataAccessException e){
			return "dataerror";
		}
			
		logger.info("in /contact/writeus post handler method ");
		return "redirect:/thanks";
	}
	
	@GetMapping("/thanks")
	public String thanksHandler(){
		logger.info("in /thanks get handler method" );
		return "thanks";
	}
	

}
