package com.newsapplication.controller;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.newsapplication.model.Article;
import com.newsapplication.model.Category;
import com.newsapplication.service.NewsServiceInterface;

@RestController
@RequestMapping("/api/news")
public class NewsRestController {
	
	Logger logger = LoggerFactory.getLogger(NewsRestController.class);
	NewsServiceInterface newsService ;
	
	@Autowired
	public void setNewsService(NewsServiceInterface newsServ){
		this.newsService = newsServ;
	}

	
	@GetMapping("/all")
	public List<Article> getLocalArticles(Locale local){
		logger.info("/api/news/all get handler method");
		return newsService.getTopHeadlineNewsByLocale(local);
	}
	
	@GetMapping("/category/{categoryname}")
	public List<Article> getLocalArticlesByCategory(Locale local, @PathVariable Category categoryname){
		logger.info("/api/news/category/" +categoryname.toString() + " get handler method");
		return newsService.getTopHeadlineNewsByLocaleAndCategory(local, categoryname);
	}
	

}
