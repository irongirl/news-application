package com.newsapplication.controller;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.newsapplication.model.Article;
import com.newsapplication.model.Category;
import com.newsapplication.service.NewsServiceInterface;

@Controller
@RequestMapping("/news")
public class NewsController {

	Logger logger = LoggerFactory.getLogger(NewsRestController.class);
	NewsServiceInterface newsService ;
	
	@Autowired
	public void setNewsService(NewsServiceInterface newsServ){
		this.newsService = newsServ;
	}

	@GetMapping("/all")
	public String getLocalArticles(Locale local, Model model){
		List<Article> articles = newsService.getTopHeadlineNewsByLocale(local);
		model.addAttribute("articles", articles);
		logger.info("in /news/all get handler method");
		return "news";
	}
	
	@GetMapping("/category/{categoryname}")
	public String getLocalArticlesByCategory(Locale local, @PathVariable Category categoryname, Model model){
		List<Article> articles = newsService.getTopHeadlineNewsByLocaleAndCategory(local, categoryname);
		model.addAttribute("articles",articles);
		model.addAttribute("category",categoryname.toString());
		logger.info("in /news/category/"+ categoryname.toString() + " get handler method");
		return "news-"+ categoryname.toString();
		
	}
	
}
